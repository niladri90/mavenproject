package com.testmaven.test;

import org.testng.annotations.Test;

public class Class_Test {

	@Test
	public void testMethod() {
		
		System.out.println("test method one");
	}
	
	@Test(priority=1)
    public void secondtestMethod() {
		
		System.out.println("New test method one two");
	}
	
	@Test(priority=2)
    public void thirdTestMetho() {
		
		System.out.println("New test method one two");
	}
	
	@Test(priority=3,enabled = false)
    public void fourthTestMetho() {
		
		System.out.println("New test method one two three");
	}
}
